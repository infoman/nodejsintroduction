# INTRODUCCION A NODEJS

- Cuál es el estilo de programación orientada a eventos y cuáles son las
ventajas de su usarlo.

- Cómo Nodejs y JavaScript hacen que la programación orientada a eventos sea fácil.

La programación tradicional hace que I/O (INPUT/OUTPUT) de la misma forma que lo hacen las llamadas a funciones locales: El proceso no puede finalizar hasta que una operación termine. Este modelo de programación de bloquear cuando se hace operaciones I/O, proviene de los días tempranos en el que compartir tiempo de sistema en cada proceso corresponde a un usuario. El propósito de esto es aislar a los usuarios unos de otros. En esos sistemas un usuario tipícamente necesita terminar una operación antes de decidir la siguiente operación que hará. Pero con el uso extendido de computadoras en el Internet, este modelo de ***"Un usuario, un proceso"***, no escala muy bien. Administrando muchos lugares de proceso en una carga de sistema operativo en el contexto de memoria cuesta mucho y en el rendimiento de estas tareas empieza a decaer 